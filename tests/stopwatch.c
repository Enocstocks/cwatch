#include <stdlib.h>
#include <stddef.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>
#include <stdint.h>
#include <string.h>
#include "../src/server/command.h"
#include "../src/server/stopwatch.h"

char **environ;
void *bitmap;
struct stopwatch **timers;
int limit = 8;

void test_sigevent_init(void **state) {
  struct stopwatch stopwatch = {
    .id = 0,
    .position = 0,

    .command = "echo foo",
    .state = STOPWATCH_RUNNING,

    .old_value = {
      .it_interval = { 0 },
      .it_value = { 0 }
    }
  };

  sigevent_t manual;

  manual.sigev_notify = SIGEV_THREAD;
  manual.sigev_notify_attributes = NULL;
  manual.sigev_notify_function = execute_command;
  manual.sigev_value.sival_ptr = &stopwatch;
  timer_create(CLOCK_REALTIME, &manual, &stopwatch.timer_id);

  sigevent_t to_test = sigevent_init(&stopwatch);

  assert_int_equal(manual.sigev_notify, to_test.sigev_notify);
  assert_null(to_test.sigev_notify_attributes);
  assert_ptr_equal(manual.sigev_notify_function, to_test.sigev_notify_function);
  assert_ptr_equal(manual.sigev_value.sival_ptr, to_test.sigev_value.sival_ptr);

  timer_delete(stopwatch.timer_id);
}

void test_itimerspec_init(void **state) {
  struct itimerspec time = itimerspec_init(500);

  struct itimerspec mtime = {
    .it_value.tv_sec = 500,
    .it_interval = {
      .tv_nsec = 0,
      .tv_sec = 0
    }
  };

  assert_int_equal(mtime.it_value.tv_sec, time.it_value.tv_sec);
}

void itimerspec_init_w_0_seconds(void **state) {
  struct itimerspec time = itimerspec_init(0);

  struct itimerspec mtime = {
    .it_value.tv_sec = 0,
    .it_interval = {
      .tv_nsec = 0,
      .tv_sec = 0
    }
  };

  assert_int_equal(mtime.it_value.tv_sec, time.it_value.tv_sec);
}

static int teardown(void **state) {
  free(bitmap);

  for (int i = 0; i < limit; i++)
    free(timers[i]);

  free(timers);

  return 0;
}

void test_find_stopwatch(void **state) {
  struct stopwatch *sw = malloc(sizeof(struct stopwatch));

  sw->id = 0;
  sw->timer_id = 0;

  timers[6] = sw;
  set_bit(bitmap, 6);

  int s = find_stopwatch(0);

  assert_int_equal(6, s);
  assert_int_equal(0, timers[s]->id);
}

void test_find_stopwatch_not_found(void **state) {
  unset_bit(bitmap, 6);
  assert_int_equal(-1, find_stopwatch(0));
}

void test_find_stopwatch_not_found_stowatch_set(void **state) {
  set_bit(bitmap, 6);
  assert_int_equal(-1, find_stopwatch(99));
}

int main(void) {
  bitmap = malloc(1);
  timers = calloc(limit, sizeof(struct stopwatch));

  const struct CMUnitTest tests[] = {
    cmocka_unit_test(test_sigevent_init),
    cmocka_unit_test(test_itimerspec_init),
    cmocka_unit_test(itimerspec_init_w_0_seconds),
    cmocka_unit_test(test_find_stopwatch),
    cmocka_unit_test(test_find_stopwatch_not_found),
    cmocka_unit_test(test_find_stopwatch_not_found_stowatch_set)
  };

  return cmocka_run_group_tests(tests, NULL, teardown);
}
