#include <stdlib.h>
#include <stddef.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>
#include <stdint.h>
#include <string.h>
#include "../src/server/command.h"

void test_parse_commad_4(void **state) {
  static char *command = "cwatch -h 2 'echo foo'";
  char **argv;

  int s = parse_command(command, &argv);
  assert_int_equal(s, 5);
  assert_string_equal(argv[0], "cwatch");
  assert_string_equal(argv[1], "-h");
  assert_string_equal(argv[2], "2");
  assert_string_equal(argv[3], "'echo foo'");
  assert_null(argv[4]);

  for (int i = 0; i < s; i++)
    free(argv[i]);
}

void test_parse_commad_5(void **state) {
  static char *command = "echo foo bar baz foobar";
  char **argv;

  int s = parse_command(command, &argv);
  assert_int_equal(s, 6);
  assert_string_equal(argv[0], "echo");
  assert_string_equal(argv[1], "foo");
  assert_string_equal(argv[2], "bar");
  assert_string_equal(argv[3], "baz");
  assert_string_equal(argv[4], "foobar");
  assert_null(argv[5]);

  for (int i = 0; i < s; i++)
    free(argv[i]);
}

int main(void) {
  const struct CMUnitTest tests[] = {
    cmocka_unit_test(test_parse_commad_4),
    cmocka_unit_test(test_parse_commad_5)
  };

  return cmocka_run_group_tests(tests, NULL, NULL);
}
