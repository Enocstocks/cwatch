#include <stdlib.h>
#include <stddef.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>
#include <stdint.h>
#include <string.h>
#include "../src/server/server.h"

#define init_socket()   serverfd = socket(PF_LOCAL, SOCK_STREAM, 0);    \
  address.sun_family = AF_LOCAL;                                        \
  strcpy(address.sun_path, SOCKET);                                     \
  if (connect(serverfd, (struct sockaddr *) &address, sizeof(struct sockaddr_un)) < 0) \
    perror("Socket connection");

#define send_message() if (send(serverfd, &msg, sizeof(struct message), 0) == -1) \
    perror("Send message");


int serverfd;
struct sockaddr_un address;
struct message msg;

static int setup(void **state) {
  switch (fork()) {
  case 0:
    start_server();
    exit(0);
    break;

  case -1:
    perror("Fork");
    exit(1);
    break;

  default:
    break;
  }

  serverfd = socket(PF_LOCAL, SOCK_STREAM, 0);
  address.sun_family = AF_LOCAL;
  strcpy(address.sun_path, SOCKET);

  // The child takes more time to start than the parent, so the parent loops
  // until a connection can be established
  while (connect(serverfd, (struct sockaddr *) &address, sizeof(struct sockaddr_un)) < 0) { }

  msg.action = CWATCH_CREATE;
  strcpy(msg.create.command, "echo foo");
  msg.create.seconds = 1000;

  send_message();
  close(serverfd);
  return 0;
}

static int teardown(void **state) {
  init_socket();

  msg.action = CWATCH_EXIT;

  send_message();

  return 0;
}

void test_info_request(void **state) {
  init_socket();

  msg.action = CWATCH_INFO;
  send_message();

  read(serverfd, &msg, sizeof(struct message));
  assert_int_equal(0, msg.info.id);
  assert_string_equal("echo foo", msg.info.command);
  assert_int_equal(STOPWATCH_RUNNING, msg.info.state);
  assert_in_range(msg.info.time_left, 0, 1000);

  read(serverfd, &msg, sizeof(struct message));
  assert_int_equal(CWATCH_END_MESSAGE, msg.action);

  close(serverfd);
}

void test_stop_stopwatch(void **state) {
  init_socket();
  msg.action = CWATCH_STOP;
  msg.stop_id = 0;
  send_message();
  read(serverfd, &msg, sizeof(struct message));
  assert_int_equal(CWATCH_SUCCESFUL_ACTION, msg.action);
  close(serverfd);

  init_socket();
  msg.action = CWATCH_INFO;
  send_message();
  read(serverfd, &msg, sizeof(struct message));
  assert_int_equal(STOPWATCH_STOPPED, msg.info.state);
  read(serverfd, &msg, sizeof(struct message));
  close(serverfd);
}

void test_run_stopwatch(void **state) {
  init_socket();
  msg.action = CWATCH_STOP;
  msg.stop_id = 0;
  send_message();
  read(serverfd, &msg, sizeof(struct message));
  assert_int_equal(CWATCH_SUCCESFUL_ACTION, msg.action);
  close(serverfd);

  init_socket();
  msg.action = CWATCH_INFO;
  send_message();
  read(serverfd, &msg, sizeof(struct message));
  assert_int_equal(STOPWATCH_RUNNING, msg.info.state);
  read(serverfd, &msg, sizeof(struct message));
  close(serverfd);
}

void test_unsuccesful_toggle(void **state) {
  init_socket();
  msg.action = CWATCH_STOP;
  msg.stop_id = 99;
  send_message();
  read(serverfd, &msg, sizeof(struct message));
  assert_int_equal(CWATCH_UNSUCCESFUL_ACTION, msg.action);
  close(serverfd);
}

void remove_stopwatch(void **state) {
  init_socket();
  msg.action = CWATCH_REMOVE;
  msg.stop_id = 0;
  send_message();
  read(serverfd, &msg, sizeof(struct message));

  assert_int_equal(msg.action, CWATCH_SUCCESFUL_ACTION);
}

int main(void) {
  const struct CMUnitTest tests[] = {
    cmocka_unit_test(test_info_request),
    cmocka_unit_test(test_stop_stopwatch),
    cmocka_unit_test(test_run_stopwatch),
    cmocka_unit_test(test_unsuccesful_toggle),
    cmocka_unit_test(remove_stopwatch)
  };

  return cmocka_run_group_tests(tests, setup, teardown);
}
