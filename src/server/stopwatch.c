#include "stopwatch.h"

extern char **environ;
extern char *bitmap;
extern struct stopwatch **timers;
extern int limit;

sigevent_t sigevent_init(struct stopwatch *timer_data){
  sigevent_t evp;
  evp.sigev_notify = SIGEV_THREAD;
  evp.sigev_notify_attributes = NULL;
  evp.sigev_notify_function = execute_command;
  evp.sigev_value.sival_ptr = timer_data;
  return evp;
}

struct itimerspec itimerspec_init(int seconds){
  struct itimerspec time;
  time.it_interval.tv_nsec = 0;
  time.it_interval.tv_sec = 0;
  time.it_value.tv_nsec = 0;
  time.it_value.tv_sec = seconds;
  return time;
}

int find_stopwatch (stopwatch_id search_id) {
  int found = 0;

  while (1) {
    found = find_set_bit(bitmap, found, limit);

    if (found == -1) break;

    if (timers[found]->id == search_id) break;

    found += 1;
  }

  return found;

}

void execute_command(union sigval command){
  char** argv;
  struct stopwatch* stopwatch_data = command.sival_ptr;
  int count = parse_command(stopwatch_data->command, &argv);
  pid_t pid;

  if (posix_spawnp(&pid, argv[0], NULL, NULL, argv, environ) != 0){
    printf("Command not found: %s", argv[0]);
    perror("SERVER ERROR");
    exit(1);
  }

  waitpid(pid, NULL, 0);

  for (int i = 0; i < count; i++)
    free(argv[i]);

  free(argv);

  timer_delete(stopwatch_data->timer_id);
  unset_bit(bitmap, stopwatch_data->position);
  memset(timers[stopwatch_data->position], 0, sizeof(struct stopwatch));
  free(stopwatch_data);
}
