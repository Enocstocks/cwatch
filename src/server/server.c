#include "server.h"

static void socket_cleanup(void);

char *bitmap;
struct stopwatch **timers;
int limit;

#define pserver_error perror("SERVER ERROR");   \
  exit(1)

void start_server(){
  static int socketfd, clientfd, bitmap_slot;
  struct sockaddr_un address;
  struct itimerspec timer_time;
  struct message message;
  stopwatch_id timers_count;
  sigevent_t evp;

  limit = 8;
  timers = malloc(sizeof(void *) * limit);
  bitmap = malloc(1);
  timers_count = 0;

  printf("Starting server...\n");
  fflush(stdout);

  setsid();

  if ((freopen("/tmp/cwatch.log", "w", stdout)) == NULL){
    pserver_error;
  }

  if ((freopen("/tmp/cwatch.log", "w", stderr)) == NULL){
    pserver_error;
  }

  if ((socketfd = socket(PF_LOCAL, SOCK_STREAM, 0)) < 0) {
    pserver_error;
  }

  if (atexit(socket_cleanup) != 0){
    pserver_error;
  }

  memset(&address, 0, sizeof(struct sockaddr_un));

  address.sun_family = AF_LOCAL;
  strcpy(address.sun_path, SOCKET);

  if ((bind(socketfd, (struct sockaddr *) &address, sizeof(struct sockaddr_un))) != 0){
    pserver_error;
  }

  if (listen(socketfd, 5) != 0) {
    pserver_error;
  }

  while (1) {
    bitmap_slot = 0;
    clientfd = accept(socketfd, NULL, NULL);

    read(clientfd, &message, sizeof(struct message));

    if (message.action == CWATCH_EXIT) {
      fprintf(stdout, "Server is going down...\n");
      break;
    }

    if (message.action == CWATCH_INFO) {

      while (bitmap_slot != -1) {

        bitmap_slot = find_set_bit(bitmap, bitmap_slot, limit);

        if (bitmap_slot == -1) {

          fprintf(stdout, "[INFO] Ending stream of timers information\n");

          message.action = CWATCH_END_MESSAGE;

        } else {

          fprintf(stdout, "[INFO] sending information of timer %d\n", timers[bitmap_slot]->id);

          memset(&message.info.command, 0, 100);
          message.info.id = timers[bitmap_slot]->id;
          strcpy(message.info.command, timers[bitmap_slot]->command);
          message.info.state = timers[bitmap_slot]->state;

          if (timers[bitmap_slot]->state == STOPWATCH_STOPPED) {
            message.info.time_left = timers[bitmap_slot]->old_value.it_value.tv_sec;
          } else {
            timer_gettime(timers[bitmap_slot]->timer_id, &timer_time);
            message.info.time_left = timer_time.it_value.tv_sec;
          }

          message.action = 0;

          bitmap_slot += 1; // avoid infinite loop in the same bitmap space
        }

        write(clientfd, &message, sizeof(struct message));
      }
    }

    if (message.action == CWATCH_CREATE){
      bitmap_slot = find_unset_bit(bitmap, 0, limit);

      // Increase bitmap size if all spaces in bitmap are in use
      if (bitmap_slot == -1){
        limit += 8;
        timers = realloc(timers, sizeof(void *) * limit);
        bitmap = realloc(bitmap, (limit / 8));
        bitmap_slot = limit - 8;
        fprintf(stdout, "[INFO] New size of bitmap: %d", limit);
      }

      // Fill stopwatch structure
      timers[bitmap_slot] = malloc(sizeof(struct stopwatch));
      strcpy(timers[bitmap_slot]->command, message.create.command);
      timers[bitmap_slot]->position = bitmap_slot;
      timers[bitmap_slot]->id = timers_count;
      fprintf(stdout, "[INFO] timer %d created with %ld seconds\n", timers_count, message.create.seconds);
      timers_count += 1;

      evp = sigevent_init(timers[bitmap_slot]);
      timer_time = itimerspec_init(message.create.seconds);

      timer_create(CLOCK_REALTIME, &evp, &timers[bitmap_slot]->timer_id);
      set_bit(bitmap, bitmap_slot);

      timer_settime(timers[bitmap_slot]->timer_id, 0, &timer_time, NULL);
    }

    // Toggle between stoped/running state of a timer
    if (message.action == CWATCH_STOP) {

      message.action = CWATCH_SUCCESFUL_ACTION;

      if ((bitmap_slot = find_stopwatch(message.stop_id)) == -1){
        message.action = CWATCH_UNSUCCESFUL_ACTION;
        fprintf(stdout, "[INFO] unsuccesful toggle, timer %d does not exists\n", message.stop_id);

      } else if (timers[bitmap_slot]->state == STOPWATCH_STOPPED) {
        timers[bitmap_slot]->state = STOPWATCH_RUNNING;
        timer_settime(timers[bitmap_slot]->timer_id, 0, &timers[bitmap_slot]->old_value, NULL);
        fprintf(stdout, "[INFO] Timer %d is now running\n", message.stop_id);

      } else {
        timer_time = itimerspec_init(0);
        timers[bitmap_slot]->state = STOPWATCH_STOPPED;
        timer_settime(timers[bitmap_slot]->timer_id, 0, &timer_time, &timers[bitmap_slot]->old_value);
        fprintf(stdout, "[INFO] Timer %d stopped\n", message.stop_id);
      }

      write(clientfd, &message, sizeof(struct message));
    }

    if (message.action == CWATCH_REMOVE) {
      if ((bitmap_slot = find_stopwatch(message.stop_id)) == -1) {
        message.action = CWATCH_UNSUCCESFUL_ACTION;
        fprintf(stdout, "[INFO] Timer %d does not exist, it cannot be deleted\n", message.stop_id);
      } else {
        message.action = CWATCH_SUCCESFUL_ACTION;
        timer_delete(timers[bitmap_slot]->timer_id);
        memset(timers[bitmap_slot], 0, sizeof(struct stopwatch));
        unset_bit(bitmap, bitmap_slot);
        free(timers[bitmap_slot]);
        fprintf(stdout, "[INFO] Timer %d deleted\n", message.stop_id);
      }

      write(clientfd, &message, sizeof(struct message));
    }

    fflush(stdout);
    close(clientfd);
  }

  close(socketfd);
  free(timers);
  free(bitmap);
}

static void socket_cleanup(){
  if (remove(SOCKET)){
    pserver_error;
  }
}
