#include <time.h>
#include <signal.h>
#include <spawn.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include "command.h"
#include "bitmap/lib/bitmap.h"
#include "../common/protocol.h"

#define STOPWATCH_RUNNING  0
#define STOPWATCH_STOPPED  1

struct stopwatch {
  stopwatch_id id;
  int position;
  timer_t timer_id;
  char command[100];
  char state;
  struct itimerspec old_value;
};

sigevent_t sigevent_init(struct stopwatch *timer_data);

struct itimerspec itimerspec_init(int seconds);

int find_stopwatch (stopwatch_id id);

void execute_command(union sigval command);
