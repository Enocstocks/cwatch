#include <unistd.h>
#include <sys/socket.h>
#include <string.h>
#include <sys/un.h>
#include "stopwatch.h"
#include "bitmap/lib/bitmap.h"
#include "../common/protocol.h"

#define SOCKET "/tmp/cwatch-sock"

void start_server(void);
