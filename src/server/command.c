#include "command.h"

int parse_command(char *string, char ***dest) {
  char **args = malloc(sizeof(char *) * 5);
  char *i, *command_arg, *beg;
  int count = 1, command_len;

  do {

    i = (strpbrk(string, " '"));

    if (*string == '\0') break;

    if (i == NULL) {
      command_len = strlen(string) + 1;
      command_arg = malloc(command_len + 1);
      args[count-1] = strcpy(command_arg, string);
    } else if (*i == '\'') {
      beg = i;
      string = i + 1;
      i = strpbrk(string, "'");
      command_len = (i - beg) + 1;
      command_arg = malloc(command_len + 1);
      args[count-1] = strncpy(command_arg, beg, command_len);
    } else {
      command_len = i - string;
      command_arg = malloc(command_len + 1);
      args[count-1] = strncpy(command_arg, string, command_len);
    }

    // add null as the last character
    command_arg[command_len] = 0;

    // add one to the address to avoid the searched character in strpbrk
    string = i + 1;

    count += 1;

    // add more pointers re-allocating all the memory
    if (count > 5){
      args = realloc(args, (count) * sizeof(char *));

      if (args == NULL){
        perror("ERROR: realloc");
        exit(1);
      }
    }
  } while (i != NULL);

  *dest = args;

  if (count <= 5)
    return 5;
  else
    return count;

}
