#include <argp.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "../common/protocol.h"

struct arguments {
  int hours, minutes, seconds;
  char *command;
  stopwatch_id target_timer;
  bool info;
  bool pause;
  bool exit;
  bool remove;
};

error_t parse_opts(int key, char *arg, struct argp_state *state);
