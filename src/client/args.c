#include "args.h"

const char* argp_program_version = "cwatch 0.01";

const char* argp_program_bug_address = "<masterenoc@tutamail.com> or check the codeberg repo";

struct argp_option opts[8] = {
  { "hours", 'h', "hours", 0, "Hours before expiration for a new stopwatch", 0 },
  { "minutes", 'm', "minutes", 0, "Minutes before expiration for a new stopwatch", 0 },
  { "seconds", 's', "seconds", 0, "Seconds before expiration for a new stopwatch", 0 },
  { 0, 'g', "timer_id", 0, "Toggle running/stopped state of a stopwatch", 0 },
  { 0, 'i', 0, OPTION_ARG_OPTIONAL, "Query information of the cwatch server", 0 },
  { 0, 'x', 0, OPTION_ARG_OPTIONAL, "Shutdown server", 0 },
  { 0, 'd', "timer_id", 0, "Delete stopwatch with timer_id", 0 },
  { 0 }
};

const struct argp argp = {
  opts,
  parse_opts,
  0,
  "cwatch [OPTIONS] [COMMAND]"
};

error_t parse_opts(int key, char *arg, struct argp_state *state) {
  struct arguments *args = state->input;

  switch (key) {

  case 'h':
    args->hours = atoi(arg);
    break;

  case 'm':
    args->minutes = atoi(arg);
    break;

  case 's':
    args->seconds = atoi(arg);
    break;

  case 'i':
    args->info = true;
    break;

  case 'g':
    args->pause = true;
    args->target_timer = atoi(arg);
    break;

  case 'x':
    args->exit = true;
    break;

  case 'd':
    args->remove = true;
    args->target_timer = atoi(arg);
    break;

  case ARGP_KEY_ARG:
    args->command = arg;
    break;

  default:
    return ARGP_ERR_UNKNOWN;
    break;
  }

  return 0;
}
