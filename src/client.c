#include <stdio.h>
#include <unistd.h>
#include <spawn.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include "client/args.h"
#include "server/server.h"

#define pclient_error() perror("CLIENT ERROR"); \
  exit(1)

#define send_message(a) if ((message_len = send(serverfd, &message, sizeof(a), 0)) < 0) { \
    pclient_error();                                                      \
  }

extern struct argp argp;

extern char **environ;

int main (int argc, char *argv[]) {
  struct arguments args = { 0 };
  struct stat sock_stats;
  struct sockaddr_un address;
  struct message message;
  int serverfd;
  pid_t command_pid;
  ssize_t message_len;
  char *state_string;

  argp_parse(&argp, argc, argv, 0, 0, &args);

  // show message if server is running but the cwatch command is executed w/o args
  if (stat(SOCKET, &sock_stats) == 0 && argc == 1){
    printf("Server is running, query or start a stopwatch\n");
    exit(0);
  }

  // create server
  if (stat(SOCKET, &sock_stats) == -1){
    switch (command_pid = fork()){

    case 0:
      start_server();
      return 0;

    default:
      return 0;
    }
  }

  serverfd = socket(PF_LOCAL, SOCK_STREAM, 0);

  memset(&address, 0, sizeof(struct sockaddr_un));

  address.sun_family = AF_LOCAL;
  strcpy(address.sun_path, SOCKET);

  if (connect(serverfd, (struct sockaddr *) &address, sizeof(struct sockaddr_un)) < 0) {
    pclient_error();
  }

  // client requests
  memset(&message, 0, sizeof(struct message));

  if (args.exit) {
    message.action = CWATCH_EXIT;
    send_message(uint8_t);
    exit(0);
  }

  if (args.info) {
    message.action = CWATCH_INFO;
    send_message(uint8_t);

    while (1) {
      read(serverfd, &message, sizeof(struct message));

      if (message.action == CWATCH_END_MESSAGE)
        break;

      if (message.info.state == STOPWATCH_RUNNING)
        state_string = "Running";
      else
        state_string = "Stopped";

      printf("Timer id: %d\nCommand to execute: %s\nTime to expire: %d seconds\nState: %s\n\n",
             message.info.id, message.info.command, message.info.time_left, state_string);
    }

    exit(0);
  }

  if (args.pause) {
    message.action = CWATCH_STOP;
    message.stop_id = args.target_timer;

    send_message(struct message);

    read(serverfd, &message, sizeof(struct message));

    if (message.action == CWATCH_UNSUCCESFUL_ACTION)
      printf("There is no stopwatch with id %d\n", args.target_timer);

    exit(0);
  }

  if (args.remove) {
    message.action = CWATCH_REMOVE;
    message.stop_id = args.target_timer;

    send_message(struct message);

    read(serverfd, &message, sizeof(struct message));

    if (message.action == CWATCH_UNSUCCESFUL_ACTION)
      printf("Stopwatch %d could not be deleted \n", args.target_timer);

    exit(0);
  }

  if (args.command == NULL)
    args.command = "echo 'Timer terminated'";

  message.action = CWATCH_CREATE;
  message.create.seconds = (args.hours * 60 * 60) + (args.minutes * 60) + args.seconds;

  if (message.create.seconds == 0)
    message.create.seconds = 60;

  strcpy(message.create.command, args.command);

  send_message(struct message);
}
