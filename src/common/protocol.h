#ifndef PROTOCOL_CWATCH

#include <stdint.h>

#define PROTOCOL_CWATCH

#define CWATCH_EXIT 0
#define CWATCH_INFO 1
#define CWATCH_STOP 2
#define CWATCH_CREATE 4
#define CWATCH_REMOVE 8

#define CWATCH_SUCCESFUL_ACTION   0
#define CWATCH_UNSUCCESFUL_ACTION 1

#define CWATCH_END_MESSAGE 1

typedef int stopwatch_id;

// "c" prefix stands for "create"
struct cstopwatch {
  long seconds;
  char command[100];
};

// "i" prefix stands for "info"
struct istopwatch {
  unsigned int id;
  char command[100];
  int time_left;
  uint8_t state;
};

struct message {
  uint8_t action;

  union {
    struct cstopwatch create;
    struct istopwatch info;
    stopwatch_id stop_id;
  };
};

#endif
